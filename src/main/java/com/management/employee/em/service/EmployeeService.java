package com.management.employee.em.service;

import com.management.employee.em.dto.EmployeeDTO;
import com.management.employee.em.dto.EmployeeResponseDTO;
import com.management.employee.em.entity.Employee;
import com.management.employee.em.entity.Position;
import com.management.employee.em.exception.CustomMessage;
import com.management.employee.em.repository.EmployeeRepository;
import com.management.employee.em.repository.PositionRepository;
import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class EmployeeService {

    Logger logger = LoggerFactory.getLogger(EmployeeService.class);

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private PositionRepository positionRepository;

    @Transactional
    public ResponseEntity<CustomMessage> listAllEmployees(String page, String size) {
        try {
            System.out.println("[PRINT] Page: " + page + ", Size: " + size );
            Pageable pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size), Sort.by("createdDate").descending());
            Page<EmployeeResponseDTO> result = employeeRepository.findAllEmployeesDetail(pageable);
            return new ResponseEntity<>(new CustomMessage("All employees data : ", false, result), HttpStatus.OK);
        } catch (Exception ex) {
            logger.trace("[LOG]ERROR!");
            ex.printStackTrace();
            String message = ex.getMessage();
            return new ResponseEntity<>(new CustomMessage("Warning! [" + message + "]", true, null), HttpStatus.BAD_REQUEST);
        }
    }

    @Transactional
    public ResponseEntity<CustomMessage> employeeDetail(EmployeeDTO request) {
        try {
            if (request.getId() != null) {
                System.out.println("[PRINT] request.getId(): " + request.getId());
//                Optional<Employee> optionalEmployeeObj = employeeRepository.findById(request.getId());
                EmployeeResponseDTO employeeObj = employeeRepository.findEmployeeDetailById(request.getId());
                return new ResponseEntity<>(new CustomMessage("Employee data: ", false, employeeObj), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(new CustomMessage("We can't process your data. Please try again!", true, request.getId()), HttpStatus.BAD_REQUEST);
            }
        } catch (Exception ex) {
            logger.trace("[LOG]ERROR!");
            ex.printStackTrace();
            String message = ex.getMessage();
            return new ResponseEntity<>(new CustomMessage("Warning! [" + message + "]", true, null), HttpStatus.BAD_REQUEST);
        }
    }

    @Transactional
    public ResponseEntity<CustomMessage> addNewEmployee(EmployeeDTO request) {
        try {
            if (request.getFirstName() != null) {
                System.out.println("[PRINT] request: " + request);
                Employee employee = new Employee();
                employee.setCreatedDate(LocalDateTime.now().plusHours(7));
                employee.setFirstName(request.getFirstName());
                employee.setLastName(request.getLastName());
                employee.setNik(request.getNik());
                employee.setCurrPositionId(request.getCurrPositionId());
                employee.setCurrDepartmentId(request.getCurrDepartmentId());
                System.out.println("[PRINT] employee: " + employee);

                employeeRepository.save(employee);
                return new ResponseEntity<>(new CustomMessage("Add new employee successfully!", false, employee), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(new CustomMessage("We can't process your data. Please try again!", true, ""), HttpStatus.BAD_REQUEST);
            }
        } catch (Exception ex) {
            logger.trace("[LOG]ERROR!");
            ex.printStackTrace();
            String message = ex.getMessage();
            return new ResponseEntity<>(new CustomMessage("Warning! [" + message + "]", true, null), HttpStatus.BAD_REQUEST);
        }
    }

    @Transactional
    public ResponseEntity<CustomMessage> promoteDemoteEmployee(EmployeeDTO request, String action) {
        try {
            System.out.println("[PRINT] request.getId(): " + request.getId());
            if (request.getId() != null) {
//                FIND BY DATA
//                Employee empObj = employeeRepository.findByFirstName(request.getFirstName());
//                System.out.println("[PRINT] empObj(): " + empObj.getFirstName());

                AtomicReference<String> message = new AtomicReference<>("");
                Optional<Employee> optionalEmployeeObj = employeeRepository.findById(request.getId());
                System.out.println("[PRINT] optionalEmployeeObj: " + optionalEmployeeObj.get().getFirstName());
                Optional<Position> optionalPositionObj = positionRepository.findById(optionalEmployeeObj.get().getCurrPositionId());
                System.out.println("[PRINT] optionalPositionObj: " + optionalPositionObj.get().getName());

                optionalEmployeeObj.ifPresent(i -> {
                    Employee employeeObj = new Employee();

                    employeeObj = optionalEmployeeObj.get();
                    System.out.println("[PRINT] employeeObj: " + employeeObj);
                    employeeObj.setModifiedDate(LocalDateTime.now().plusHours(7));
                    Position positionPromoteObj = positionRepository.findByRank(optionalPositionObj.get().getRank()+1);
                    Position positionDemoteObj = positionRepository.findByRank(optionalPositionObj.get().getRank()-1);
                    if(action == "promote" && positionPromoteObj != null){
                        employeeObj.setPrevPositionId(employeeObj.getCurrPositionId());
                        employeeObj.setCurrPositionId(positionPromoteObj.getId());
                        message.set("Employee " + optionalEmployeeObj.get().getFirstName() + " successfully " + action + "!");
                    } else if (action == "demote" && positionDemoteObj != null){
                        employeeObj.setPrevPositionId(employeeObj.getCurrPositionId());
                        employeeObj.setCurrPositionId(positionDemoteObj.getId());
                        message.set("Employee " + optionalEmployeeObj.get().getFirstName() + " successfully " + action + "!");
                    } else {
                        message.set("Employee " + optionalEmployeeObj.get().getFirstName() + " can't be promote/demote!");
                    }
                    employeeRepository.save(employeeObj);
                    System.out.println("[PRINT] employeeObj(NOW): " + employeeObj);
                });
                return new ResponseEntity<>(new CustomMessage(message.get(), false, optionalEmployeeObj.get().getFirstName()), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(new CustomMessage("We can't process your data. Please try again!", true, ""), HttpStatus.BAD_REQUEST);
            }
        } catch (Exception ex) {
            logger.trace("[LOG]ERROR!");
            ex.printStackTrace();
            String message = ex.getMessage();
            return new ResponseEntity<>(new CustomMessage("Warning! [" + message + "]", true, null), HttpStatus.BAD_REQUEST);
        }
    }

    @Transactional
    public ResponseEntity<CustomMessage> updateEmployee(EmployeeDTO request) {
        try {
            System.out.println("[PRINT] request.getId(): " + request.getId());
            if (request.getId() != null) {
                Optional<Employee> optionalEmployeeObj = employeeRepository.findById(request.getId());
                System.out.println("[PRINT] optionalEmployeeObj: " + optionalEmployeeObj.get().getFirstName());

                optionalEmployeeObj.ifPresent(i -> {
                    Employee employeeObj = new Employee();

                    employeeObj = optionalEmployeeObj.get();
                    System.out.println("[PRINT] employeeObj: " + employeeObj);
                    employeeObj.setModifiedDate(LocalDateTime.now().plusHours(7));
                    employeeObj.setFirstName(request.getFirstName());
                    employeeObj.setLastName(request.getLastName());
                    employeeObj.setNik(request.getNik());
                    employeeObj.setCurrPositionId(request.getCurrPositionId());
                    employeeObj.setCurrDepartmentId(request.getCurrDepartmentId());
                    employeeObj.setPrevPositionId(request.getPrevPositionId());
                    employeeObj.setPrevDepartmentId(request.getPrevDepartmentId());
                    employeeRepository.save(employeeObj);
                    System.out.println("[PRINT] employeeObj(NOW): " + employeeObj);
                });
                return new ResponseEntity<>(new CustomMessage("Employee " + optionalEmployeeObj.get().getFirstName() + " successfully updated!", false, optionalEmployeeObj.get().getFirstName()), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(new CustomMessage("We can't process your data. Please try again!", true, ""), HttpStatus.BAD_REQUEST);
            }
        } catch (Exception ex) {
            logger.trace("[LOG]ERROR!");
            ex.printStackTrace();
            String message = ex.getMessage();
            return new ResponseEntity<>(new CustomMessage("Warning! [" + message + "]", true, null), HttpStatus.BAD_REQUEST);
        }
    }
}
