package com.management.employee.em.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

@Data
//@AllArgsConstructor
@NoArgsConstructor
@ToString
public class EmployeeResponseDTO {

    private String id;
    private String firstName;
    private String lastName;
    private String nik;
    private String currPositionName;
    private String prevPositionName;
    private String currDepartmentName;
    private String prevDepartmentName;

    public EmployeeResponseDTO(String id, String firstName, String lastName, String nik,
                               String currPositionName, String prevPositionName,
                               String currDepartmentName, String prevDepartmentName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.nik = nik;
        this.currPositionName = currPositionName;
        this.prevPositionName = prevPositionName;
        this.currDepartmentName = currDepartmentName;
        this.prevDepartmentName = prevDepartmentName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getCurrPositionName() {
        return currPositionName;
    }

    public void setCurrPositionName(String currPositionName) {
        this.currPositionName = currPositionName;
    }

    public String getPrevPositionName() {
        return prevPositionName;
    }

    public void setPrevPositionName(String prevPositionName) {
        this.prevPositionName = prevPositionName;
    }

    public String getCurrDepartmentName() {
        return currDepartmentName;
    }

    public void setCurrDepartmentName(String currDepartmentName) {
        this.currDepartmentName = currDepartmentName;
    }

    public String getPrevDepartmentName() {
        return prevDepartmentName;
    }

    public void setPrevDepartmentName(String prevDepartmentName) {
        this.prevDepartmentName = prevDepartmentName;
    }
}
