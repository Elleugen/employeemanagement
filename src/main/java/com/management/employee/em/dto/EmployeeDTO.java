package com.management.employee.em.dto;

import com.management.employee.em.entity.Employee;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

@Data
//@AllArgsConstructor
@NoArgsConstructor
@ToString
public class EmployeeDTO {

    private String id;
    private LocalDateTime createdDate;
    private String firstName;
    private String lastName;
    private String nik;

    private String currPositionId;
    private String currDepartmentId;
    private String prevPositionId;
    private String prevDepartmentId;


    public EmployeeDTO(String id, LocalDateTime createdDate, String firstName, String lastName, String nik,
                       String currPositionId, String currDepartmentId, String prevPositionId, String prevDepartmentId) {
        this.id = id;
        this.createdDate = createdDate;
        this.firstName = firstName;
        this.lastName = lastName;
        this.nik = nik;

        this.currPositionId = currPositionId;
        this.currDepartmentId = currDepartmentId;
        this.prevPositionId = prevPositionId;
        this.prevDepartmentId = prevDepartmentId;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCurrPositionId() {
        return currPositionId;
    }

    public void setCurrPositionId(String currPositionId) {
        this.currPositionId = currPositionId;
    }

    public String getCurrDepartmentId() {
        return currDepartmentId;
    }

    public void setCurrDepartmentId(String currDepartmentId) {
        this.currDepartmentId = currDepartmentId;
    }

    public String getPrevPositionId() {
        return prevPositionId;
    }

    public void setPrevPositionId(String prevPositionId) {
        this.prevPositionId = prevPositionId;
    }

    public String getPrevDepartmentId() {
        return prevDepartmentId;
    }

    public void setPrevDepartmentId(String prevDepartmentId) {
        this.prevDepartmentId = prevDepartmentId;
    }
}
