package com.management.employee.em.repository;

import com.management.employee.em.entity.Employee;
import com.management.employee.em.entity.Position;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PositionRepository extends JpaRepository<Position, String> {

    Position findByRank(int rank);
}
