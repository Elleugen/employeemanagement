package com.management.employee.em.repository;

import com.management.employee.em.dto.EmployeeDTO;
import com.management.employee.em.dto.EmployeeResponseDTO;
import com.management.employee.em.entity.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee, String> {

//    Page <Employee> findByEmployeeId(@Param("id") String id, Pageable pageable);

//    @Query("SELECT e from Employee e "+
//            "ORDER BY otwh.createdDate")
//    List<Employee> findAllEmployeesDetail(@Param("id") String id);

    @Query("select new com.management.employee.em.dto.EmployeeResponseDTO(e.id, e.firstName, e.lastName, e.nik, pa.name, pb.name, da.name, db.name) "
            + "from Employee e "
            + "LEFT JOIN Position pa ON e.currPositionId = pa.id "
            + "LEFT JOIN Position pb ON e.prevPositionId = pb.id "
            + "LEFT JOIN Department da ON e.currDepartmentId = da.id "
            + "LEFT JOIN Department db ON e.prevDepartmentId = db.id "
            + "ORDER BY e.firstName ASC "
    )
    Page<EmployeeResponseDTO> findAllEmployeesDetail(Pageable pageable);

//    @Query("select new com.management.employee.em.dto.EmployeeDTO(e.firstName, e.lastName, e.nik, p.name) "
//            + "from Employee e "
//            + "WHERE e.nik =:nik "
//            + "ORDER BY e.firstName")
//    EmployeeDTO findByData(String nik, String firstName, String currDepartmentId, String currPositionId);


    @Query("select new com.management.employee.em.dto.EmployeeResponseDTO(e.id, e.firstName, e.lastName, e.nik, pa.name, pb.name, da.name, db.name) "
            + "from Employee e "
            + "LEFT JOIN Position pa ON e.currPositionId = pa.id "
            + "LEFT JOIN Position pb ON e.prevPositionId = pb.id "
            + "LEFT JOIN Department da ON e.currDepartmentId = da.id "
            + "LEFT JOIN Department db ON e.prevDepartmentId = db.id "
            + "WHERE e.id =:employeeId "
    )
    EmployeeResponseDTO findEmployeeDetailById(String employeeId);

    Employee findByFirstName(String firstName);
}
