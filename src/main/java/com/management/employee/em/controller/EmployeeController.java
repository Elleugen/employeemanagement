package com.management.employee.em.controller;

import com.management.employee.em.dto.EmployeeDTO;
import com.management.employee.em.entity.Employee;
import com.management.employee.em.exception.CustomMessage;
import com.management.employee.em.repository.EmployeeRepository;
import com.management.employee.em.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.List;

@RequestMapping("/api/v1/employee")
@RestController
public class EmployeeController {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/")
    public String hi(){
        return "HelloWorld";
    }

    @GetMapping("list")
    public ResponseEntity<CustomMessage> listAllEmployees(@RequestParam(value = "page", required = false, defaultValue = "0") String page,
                                                       @RequestParam(value = "size", required = false, defaultValue = "15") String size
                                                        ) {
        System.out.println("[PRINT] listAllEmployees..");
        return employeeService.listAllEmployees(page, size);
    }

    @GetMapping("detail")
    public ResponseEntity<CustomMessage> employeeDetail(@RequestBody EmployeeDTO request
    ) {
        System.out.println("[PRINT] employeeDetail..");
        return employeeService.employeeDetail(request);
    }

    @PostMapping("add")
    public ResponseEntity<CustomMessage> addNewEmployee(@RequestBody EmployeeDTO request) {
        System.out.println("[PRINT] addNewEmployee..");
        return employeeService.addNewEmployee(request);
    }

    @PatchMapping("promote")
    public ResponseEntity<CustomMessage> promoteEmployee(@RequestBody EmployeeDTO request) {
        System.out.println("[PRINT] promoteEmployee..");
        String action = "promote";
        return employeeService.promoteDemoteEmployee(request, action);
    }

    @PatchMapping("demote")
    public ResponseEntity<CustomMessage> demoteEmployee(@RequestBody EmployeeDTO request) {
        System.out.println("[PRINT] demoteEmployee..");
        String action = "demote";
        return employeeService.promoteDemoteEmployee(request, action);
    }

    @PutMapping("update")
    public ResponseEntity<CustomMessage> updateEmployee(@RequestBody EmployeeDTO request) {
        System.out.println("[PRINT] updateEmployee..");
        return employeeService.updateEmployee(request);
    }
}
