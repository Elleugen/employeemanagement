package com.management.employee.em.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "employee")
public class Employee {

//    @Id
//    @GeneratedValue

//    @Id @GeneratedValue(generator="system-uuid")
//    @GenericGenerator(name="system-uuid", strategy = "uuid")
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private LocalDateTime createdDate;
    private LocalDateTime modifiedDate;
    private String firstName;
    private String lastName;
    private String nik;
    private String currPositionId;
    private String currDepartmentId;
    private String prevPositionId;
    private String prevDepartmentId;

////    @ManyToOne(fetch = FetchType.LAZY)
////    @JoinColumn(name = "project_jobs_id",referencedColumnName = "id",insertable = false,updatable = false)
//    @OneToOne(targetEntity = Position.class, cascade = CascadeType.ALL)
//    @JoinColumn(name = "cp_id", referencedColumnName = "id")
//    private Position position;
}
